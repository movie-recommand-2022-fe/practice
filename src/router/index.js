/*
 * @Date: 2022-06-29 14:04:06
 * @LastEditTime: 2022-06-29 14:48:31
 */
import Vue from 'vue'
import Router from 'vue-router'
// import HelloWorld from '@/components/HelloWorld'
import Users from '@/views/Users'
import jsCompotents from '@/components/JiangshuoComponents'

Vue.use(Router)

export default new Router({
  routes: [
    // {
    //   path: '/',
    //   name: 'HelloWorld',
    //   component: HelloWorld
    // },
    {
      path: '/',
      name: 'Users',
      component: Users,
      children: [ // 这个地方是一个嵌套路由，在children下的路由会嵌套在父路由下，比如下面这个路由将会是/users/a9ia
        {
          path: 'a9ia',
          name: 'JiangshuoComponents',
          component: jsCompotents
        }
      ]
    }
  ]
})
