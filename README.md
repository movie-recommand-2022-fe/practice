<!--
 * @Date: 2022-06-29 14:04:06
 * @LastEditTime: 2022-06-29 15:22:11
-->
# movie-recommand-fe-2022

> A Vue.js project

你需要一个 **nodeJS 环境 和 git 环境**来进行 vuejs 开发，如果还没有，请自行百度 nodejs、git 安装。

## 如何运行

``` bash
# 安装所有依赖包
npm install

# 将前端页面运行在 localhost:8080，如果该条指令运行完访问 localhost:8080 有页面即运行成功
npm run dev

# 将项目打包为静态资源(html, css, js)，开发时这个语句不用运行
npm run build

# 打包并查看打包时产生的报告
npm run build --report
```



## 项目目录

当你运行成功时，项目目录如下

![image-20220629145518047](https://cdn.jsdelivr.net/gh/a9ia/image/blog/image-20220629145518047.png)

## 希望大家做什么？
可能有用的指令：
```
  git status // 展示当前git的状态，如是否有更新，本地是否有文件没有添加进 git 跟踪，是否有文件没有提交
  git checkout -b xxxx // 用于创建一个xxxx新分支
  git checkout xxx // 切换到某分支
  git branch // 用于查看新的分支
  git add . // 用于将所有的文件添加进本地 git 仓库跟踪
  git commit -m "这次更新的说明信息" // 用于提交到本地 git 仓库
  git push // 用于提交到远程仓库（网上gitee的仓库）
  git merge b // 把本地 b 分支合并到当前分支
  git pull // 获取远程的更新，可能会产生冲突，这时候就需要解冲突
  git push --set-upstream origin dev // 提交一个dev分支
```

1. 从git上clone下来这个项目
2. 使用npm install 安装包
3. 从master分支上创建新分支，以@feature:name (name用自己的名字替换，比如@feature:jiangshuo)
4. 使用 npm run dev 将其跑起来
5. 在这个新分支上创建一个新的组件，在@/views/Users.vue上加一个按钮用于跳转到/users/name，并显示这个组件
6. 把写完的代码上传到git
7. 将自己的分支与dev分支merge一下，并上传到dev分支
